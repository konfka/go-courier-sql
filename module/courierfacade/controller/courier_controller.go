package controller

import (
	"context"
	"github.com/gin-gonic/gin"
	"github.com/goccy/go-json"
	"gitlab.com/konfka/go-courier/metrics"
	"gitlab.com/konfka/go-courier/module/courierfacade/service"
	"log"
	"net/http"
	"time"
)

type CourierController struct {
	courierService service.CourierFacer
	metrics        *metrics.Metrics
}

func NewCourierController(courierService service.CourierFacer, metrics *metrics.Metrics) *CourierController {
	return &CourierController{courierService: courierService, metrics: metrics}
}

func (c *CourierController) GetStatus(ctx *gin.Context) {
	// увеличить метрику на количества запросов
	c.metrics.RequestCount.Inc()
	// установить задержку в 50 миллисекунд
	time.Sleep(50 * time.Millisecond)
	startTime := time.Now()
	defer func() {
		c.metrics.OrderRequestTime.Observe(time.Since(startTime).Seconds())
	}()
	// получить статус курьера из сервиса courierService используя метод GetStatus
	status := c.courierService.GetStatus(ctx)
	// увеличить метрику количества заказов рядом с курьером
	c.metrics.OrdersNearCourier.Set(float64(len(status.Orders)))
	// отправить статус курьера в ответ
	ctx.JSON(http.StatusOK, status)
}

func (c *CourierController) MoveCourier(m webSocketMessage) {
	var cm CourierMove
	var err error

	c.metrics.RequestCount.Inc()

	start := time.Now()

	defer func() {
		c.metrics.OrderRequestTime.Observe(time.Since(start).Seconds())
	}()
	// получить данные из m.Data и десериализовать их в структуру CourierMove
	err = json.Unmarshal(m.Data.([]byte), &cm)
	if err != nil {
		log.Fatalln(err)
	}
	// вызвать метод MoveCourier у courierService
	c.courierService.MoveCourier(context.Background(), cm.Direction, cm.Zoom)
}
