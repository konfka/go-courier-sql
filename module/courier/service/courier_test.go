package service

import (
	"context"
	"github.com/stretchr/testify/mock"
	"gitlab.com/konfka/go-courier/geo"
	"gitlab.com/konfka/go-courier/module/courier/models"
	"gitlab.com/konfka/go-courier/module/courier/storage/mocks"
	"reflect"
	"testing"
)

var (
	testCourier = models.Courier{
		Score: 0,
		Location: models.Point{
			Lat: 23,
			Lng: 23,
		},
	}
)

func TestCourierService_GetCourier(t *testing.T) {
	type fields struct {
		courierStorage *mocks.CourierStorager
		allowedZone    *geo.MockPolygonChecker
		disabledZones  []geo.PolygonChecker
	}
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *models.Courier
		wantErr bool
	}{
		{
			name: "test1",
			fields: fields{
				courierStorage: mocks.NewCourierStorager(t),
				allowedZone:    geo.NewMockPolygonChecker(t),
				disabledZones:  nil,
			},
			args:    args{ctx: context.Background()},
			want:    &testCourier,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.fields.courierStorage.On("GetOne", mock.Anything).Return(tt.want, nil)
			tt.fields.courierStorage.On("Save", mock.Anything, mock.Anything).Return(nil)
			tt.fields.allowedZone.On("Contains", mock.Anything).Return(true)

			c := &CourierService{
				courierStorage: tt.fields.courierStorage,
				allowedZone:    tt.fields.allowedZone,
				disabledZones:  tt.fields.disabledZones,
			}

			got, err := c.GetCourier(tt.args.ctx)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetCourier() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetCourier() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCourierService_MoveCourier(t *testing.T) {
	type fields struct {
		courierStorage mocks.CourierStorager
		allowedZone    geo.MockPolygonChecker
		disabledZones  []geo.PolygonChecker
	}
	type args struct {
		courier   models.Courier
		direction int
		zoom      int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "test1",
			fields: fields{
				courierStorage: *mocks.NewCourierStorager(t),
				allowedZone:    *geo.NewMockPolygonChecker(t),
				disabledZones:  nil,
			},
			args: args{
				courier:   testCourier,
				direction: 1,
				zoom:      1,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.fields.courierStorage.On("Save", mock.Anything, mock.Anything).Return(nil)
			tt.fields.allowedZone.On("Contains", mock.Anything).Return(true)

			c := &CourierService{
				courierStorage: &tt.fields.courierStorage,
				allowedZone:    &tt.fields.allowedZone,
				disabledZones:  tt.fields.disabledZones,
			}
			if err := c.MoveCourier(tt.args.courier, tt.args.direction, tt.args.zoom); (err != nil) != tt.wantErr {
				t.Errorf("MoveCourier() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestCourierService_ScoreUpdate(t *testing.T) {
	type fields struct {
		courierStorage *mocks.CourierStorager
		allowedZone    *geo.MockPolygonChecker
		disabledZones  []geo.PolygonChecker
	}
	type args struct {
		courier models.Courier
		scores  int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "test",
			fields: fields{
				courierStorage: mocks.NewCourierStorager(t),
				allowedZone:    geo.NewMockPolygonChecker(t),
				disabledZones:  nil,
			},
			args: args{
				courier: testCourier,
				scores:  0,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.fields.courierStorage.On("Save", mock.Anything, mock.Anything).Return(nil)

			c := &CourierService{
				courierStorage: tt.fields.courierStorage,
				allowedZone:    tt.fields.allowedZone,
				disabledZones:  tt.fields.disabledZones,
			}
			if err := c.ScoreUpdate(tt.args.courier, tt.args.scores); (err != nil) != tt.wantErr {
				t.Errorf("ScoreUpdate() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
