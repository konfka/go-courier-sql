package storage

import (
	"context"
	"github.com/jmoiron/sqlx"
	geo "github.com/kellydunn/golang-geo"
	"gitlab.com/konfka/go-courier/module/order/models"
	"time"
)

//go:generate go run github.com/vektra/mockery/v2@v2.20.0 --name OrderStorager
type OrderStorager interface {
	Save(ctx context.Context, order models.Order, maxAge time.Duration) error                       // Сохранить заказ с временем жизни
	GetByID(ctx context.Context, orderID int) (*models.Order, error)                                // Получить заказ по id
	GetByRadius(ctx context.Context, lng, lat, radius float64, unit string) ([]models.Order, error) // Получить заказы в радиусе от точки
	GetCount(ctx context.Context) (int, error)                                                      // Получить количество заказов
	RemoveOldOrders(ctx context.Context, maxAge time.Duration) error                                // Удалить старые заказы по истечению времени maxAge
}

type OrderStorage struct {
	db *sqlx.DB
}

func NewOrderStorage(db *sqlx.DB) OrderStorager {
	return &OrderStorage{db: db}
}

func (o *OrderStorage) Save(ctx context.Context, order models.Order, maxAge time.Duration) error {
	query := `INSERT INTO orders (price, delivery_price, lng, lat, created_at) VALUES ($1, $2, $3, $4, $5)`

	_, err := o.db.Exec(query, order.Price, order.DeliveryPrice, order.Lng, order.Lat, order.CreatedAt)
	if err != nil {
		return err
	}

	return nil
}

func (o *OrderStorage) RemoveOldOrders(ctx context.Context, maxAge time.Duration) error {
	max := time.Now().Add(-maxAge)
	query := `DELETE FROM orders WHERE created_at < $1`

	_, err := o.db.Exec(query, max)
	if err != nil {
		return err
	}

	return nil
}

func (o *OrderStorage) GetByID(ctx context.Context, orderID int) (*models.Order, error) {
	var order *models.Order

	query := `SELECT * FROM orders WHERE id=$1`

	err := o.db.Get(order, query, orderID)
	if err != nil {
		return nil, err
	}
	return order, nil
}

func (o *OrderStorage) GetCount(ctx context.Context) (int, error) {
	var count int

	query := `SELECT count(*) FROM orders WHERE is_delivered=false`

	err := o.db.Get(&count, query)
	if err != nil {
		return 0, err
	}

	return count, nil
}

func (o *OrderStorage) GetByRadius(ctx context.Context, lng, lat, radius float64, unit string) ([]models.Order, error) {
	var orders []models.Order

	query := `SELECT count(*) FROM orders WHERE is_delivered=false`

	err := o.db.Select(&orders, query)
	if err != nil {
		return nil, err
	}

	res := make([]models.Order, 0, len(orders))

	for _, v := range orders {
		p1 := geo.NewPoint(lat, lng)
		p2 := geo.NewPoint(v.Lat, v.Lng)
		dist := p1.GreatCircleDistance(p2)
		if dist < radius/1000 {
			res = append(res, v)
		}
	}

	return res, nil
}
