package router

import (
	"github.com/gin-gonic/gin"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.com/konfka/go-courier/module/courierfacade/controller"
)

type Router struct {
	courier *controller.CourierController
}

func NewRouter(courier *controller.CourierController) *Router {
	return &Router{courier: courier}
}

func (r *Router) CourierAPI(router *gin.RouterGroup) {
	// прописать роуты для courier API
	router.GET("/ws", r.courier.Websocket)
	router.GET("/status", r.courier.GetStatus)
}

func (r *Router) Swagger(router *gin.RouterGroup) {
	router.GET("/swagger", swaggerUI)
	router.GET("/swagger/api/status", r.courier.GetStatus)
}

func (r *Router) Prometheus(router *gin.RouterGroup) {
	router.GET("/metrics", gin.WrapH(promhttp.Handler()))
}
