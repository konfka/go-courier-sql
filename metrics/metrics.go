package metrics

import "github.com/prometheus/client_golang/prometheus"

type Metrics struct {
	RequestCount       prometheus.Counter
	OrdersNearCourier  prometheus.Gauge
	OrderRequestTime   prometheus.Histogram
	CourierRequestTime prometheus.Histogram
}

func NewMetrics() *Metrics {
	RequestCount := prometheus.NewCounter(
		prometheus.CounterOpts{
			Namespace: "Courier",
			Name:      "http_request_count",
			Help:      "Total of http requests",
		},
	)
	OrdersNearCourier := prometheus.NewGauge(
		prometheus.GaugeOpts{
			Namespace: "Courier",
			Name:      "orders_near_courier",
			Help:      "Total of http requests",
		},
	)
	OrderRequestTime := prometheus.NewHistogram(
		prometheus.HistogramOpts{
			Namespace: "Courier",
			Name:      "http_order_request_execution_time",
			Help:      "Time needed to complete order request",
		},
	)
	CourierRequestTime := prometheus.NewHistogram(
		prometheus.HistogramOpts{
			Namespace: "Courier",
			Name:      "http_courier_request_execution_time",
			Help:      "Time needed to complete courier request",
		},
	)
	return &Metrics{
		RequestCount:       RequestCount,
		OrdersNearCourier:  OrdersNearCourier,
		OrderRequestTime:   OrderRequestTime,
		CourierRequestTime: CourierRequestTime,
	}
}
